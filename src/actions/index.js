import { HIT_MONSTER, HIT_BACK } from "../constants/action-types";

export function hitMonster(payload) {
  return { type: HIT_MONSTER, payload };
}

export function hitBack(payload) {
  return { type: HIT_BACK, payload };
}
