import React from 'react';
import { connect } from 'react-redux';

import { hitMonster, hitBack } from "../actions/index";
// import store from "../store/index";

const mapDispatchToProps = (dispatch) => {
         return {
             hitMonster: (hit) => dispatch(hitMonster(hit)),
             hitBack: (hit) => dispatch(hitBack(hit))
         };
};

const mapStateToProps = (state, ownProps) => {
    return {
        props: ownProps
    };
};

const ButtonCapacityConnect = (props) => {
    const combat = () => {
        props.hitMonster(5);
        props.hitBack({force: 5, player: props.player.id});
     };

    return (
            <button
              type="button"
              onClick={() => combat()}
              className="btn btn-success material-tooltip-main "
            >
              hit
              <i className="fas fa-bomb"></i> 5
              <i className="fas fa-fire-alt"></i> - 5
            </button>
        );
};

const ButtonCapacity =
      connect(mapStateToProps, mapDispatchToProps)(ButtonCapacityConnect);

export default ButtonCapacity;
