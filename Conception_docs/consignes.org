* DONE Présentation
Nous allons apprendre à connecter React et Redux avec =react-redux= !
=react-redux= est une librairie développée par l'équipe de Reactjs elle-même.

** Théorie
 Ce TP est largement inspiré de cette ressource tutorielle :

 - [[https://www.valentinog.com/blog/redux/][React Redux Tutorial for Beginners: The Definitive Guide (2019)]]

 Le repo github de =react-redux= se trouve ici, il contient une
 documentation très utile :

 - [[https://github.com/reduxjs/react-redux][GitHub -  reduxjs/react-redux: Official React bindings for Redux]]

 Hey au fait ! =react-redux= est utilisable aussi avec React Native

** DONE Installation
 Pour commencer vous allez cloner ce projet :

 - [[https://gitlab.com/simplon-roanne/random-rumble][Random-Rumble ·  GitLab]]

 Utilisez la commande =npm install= qui installera tous les modules
 nécessaire (=react-redux= est inclus)

** DONE Consignes
Vous avez maintenant devant vous la mini app simplifié de
Random-Rumble, celle ci est composé de divers components React que
nous vous invitons à étudier.

Ils sont tous rangé dans le dossier Components.

Observez Game.js, vous constaterez une notation différente des classes
habituelles :

 #+BEGIN_EXAMPLE javascript
     const App = () => (
       <div className="App">
         <Monster />
         <br></br>
         <section className="container-fluid">
           <PlayerList />
         </section >
       </div>
     )
 #+END_EXAMPLE

*Ceci est une déclaration fonctionnelle d'un component React, cette
fonction ne renvoie que le DOM affiché ( = render() en POO )*.

** WAITING Objectif
 - Installer react-redux pour créer le store.
 - Refactoriser tous les components en fonction pour utiliser la
   méthode connect de react-redux.
 - Lier le state des components avec le store de react-redux.
 - Créer les actions qui permettront d'agir sur le store.

* React redux setup
** DONE React store et react-redux provider

Premièrement nous allons créer le store de Redux.

#+BEGIN_EXAMPLE javascript
    // créer ce fichier dans src/store/index.js

     import { createStore } from "redux";
     import rootReducer from "../reducers/index";

     const store = createStore(rootReducer);

     export default store;
#+END_EXAMPLE

Dans la foulée nous allons créer notre reducer.

 #+BEGIN_EXAMPLE javascript
     // créer ce fichier dans src/reducers/index.js

     const initialState = {
       //TODO : complete players {} and monster{}
       players: {},
       monster : {}
     };

     function rootReducer(state = initialState, action) {
       return state;
     };

     export default rootReducer;
 #+END_EXAMPLE

Nous allons en profiter pour déplacer le state des joueurs dans le
reducer en tant qu'initialState. Nous pouvons aussi ajouter celui du
monstre qui n'existe pas encore.

*** Pour que nos components correspondent au store,
il va maintenant falloir lier leur state avec le store.

Nous allons pour cela modifier notre index.js à la racine de src et
ajouter provider de react-redux ainsi que le store.

Ajoutez ces 2 import et entourer la balise du component Game avec la
balise du Provider. Grace à cette notation, le store devient
accessible pour toute l'application.

#+BEGIN_EXAMPLE javascript
     // changement à ajouter à src/index.js

     import { Provider } from "react-redux";
     import store from "./store/index";

     /////////////////////////////////////////

     <Provider store={store}>
         <Game />
      </Provider>
#+END_EXAMPLE

** DONE React-Redux Connect
La documentation officiel de la method Connect :
[[https://react-redux.js.org/api/connect][Connect · React Redux]]

Pour commencer nous allons connecter le component =Monster.js= pour ce
faire nous allons devoir le refactoriser afin d'utiliser connect qui
s'écrit de cette manière :

#+BEGIN_EXAMPLE javascript
    // à ajouter dans src/Components/monster.js
    const Component = connect(mapState)(MyComponentConnect)
#+END_EXAMPLE

Changer la classe comme suit :

#+BEGIN_EXAMPLE javascript
// ce code représente une partie de src/Components/Monster.js
const MonsterConnect = () => (
  <section>
    <div className="container">
      <div className="row">
        <div className="card-monstre col-sm-12">
          <div id="monsterCard">
            <div className="text-center">
              <div className="row">
                <div className="col-sm-2 offset-sm-3">
                  <span className="badge badge-danger ml-2 "	
	             id="degatSpanMonster"></span>
                  <img className="img-fluid"
		            src="http://res.publicdomainfiles.com/pdf_view/67/13925387417373.png"
			    alt='monster' />
	          </div>

                <div id="comboOnMonster" className="col-sm-6">

                </div>
              </div>
            </div>

               <ProgressBar pv='800' pvMax='800' bgType='bg-danger'
		                 faType='fa-heart' barName=' : pv' />
          </div>
        </div>
      </div>
    </div>
  </section >
)
 #+END_EXAMPLE

et utiliser la méthode
[[https://react-redux.js.org/using-react-redux/connect-mapstate][Connect: Extracting Data with mapStateToProps · React Redux]]

 #+BEGIN_EXAMPLE javascript
     //allez je vous aide pour celle ci aussi : 
     const mapStateToProps = state => {
       return { monster: state.monster };
     };
 #+END_EXAMPLE

=monster= sera le state de notre component ici, il faut le passer dans
la const =MonsterConnect= afin de la définir comme ceci :

 #+BEGIN_EXAMPLE javascript
     const MonsterConnect = ({ monster }) => (
 #+END_EXAMPLE

Pour savoir si tout c'est bien passé, passez les valeurs de monster en
props pour le component ProgressBar.

#+BEGIN_EXAMPLE
     // extrait de src/Component/Monster.js

     <ProgressBar pv={TODO: assigner la valeur pv de monster} pvMax={TODO} />
#+END_EXAMPLE

Vous remarquerez que nous faisons descendre des props qui ne sont pas
liés au store dans =<ProgressBar />=. Pour les récupérer nous allons
devoir refactoriser ce composant aussi.

Il existe un deuxième argument dans mapStateToProps conçu à cet effet.

 #+BEGIN_EXAMPLE javascript
     // changement à apporter dans src/Component/ProgressBar.js

     const mapStateToProps = (state, ownProps) => {
         return {
             props: ownProps
         };
     };
     //props contient désormais les props comme un comportement classique de react 
 #+END_EXAMPLE

 il faudra ici aussi connecter le state et le component. notez que le
 =this= avant =props= n'est pas nécessaire !!

 #+BEGIN_EXAMPLE javascript
     // ce fichier représente src/Components/progressBar.js

     import React from 'react';
     import { connect } from 'react-redux';

     const mapStateToProps = (state, ownProps) => {
       //TODO
     };

     const ProgressBarConnect = ({ props }) => (

         //TODO
     )
     const ProgressBar = connect(TODO)(TODO);

     export default ProgressBar;
 #+END_EXAMPLE

*** Normalement à ce stade les barres de vie des joueurs ne doivent pas
 apprécier le changement de code... mais on va passer à la mise en
 pratique de ce que vous venez d'apprendre 
* DONE Mise en pratique
** DONE Connect PlayerList et PlayerCard
*** DONE A vous de refactoriser ces 2 components
 - PlayerConnect devra avoir une méthode displayPlayer() sous forme d'une
   fonction fléché, la syntax changera légerement :

 #+BEGIN_EXAMPLE javascript
     const PlayerConnect = () => {
      
       const displayPlayers = () => {
         //your code
       }

       return (
         //the render
       );
     }
 #+END_EXAMPLE

 - vous devrez utiliser ownProps pour faire passer chaque player dans les
   PlayerCard
 - aidez vous fortement de ce que nous avons mis en application avec
   =Monster.js=

* Actions

Maintenant que nous avons appris à utiliser le store, nous allons le
modifier !

** DONE Création d'actions

Vous allez créer un nouveau dossier comme suit :

 #+BEGIN_EXAMPLE javascript
     // src/actions/index.js

     export function hitMonster(payload) {
       return { type: "HIT_MONSTER", payload }
     };
 #+END_EXAMPLE

=payload= contiendra ici les valeurs de notre action, ça peut être une
valeur simple ou un array ou un objet.

** DONE Utilisation dans le Reducer
 
Nous allons maintenant intégrer notre action dans le reducer, on peut
utiliser des if ou des case :

 #+BEGIN_EXAMPLE javascript
     // modification du fichier src/reducers/index.js

     function rootReducer(state = initialState, action) {
       if (action.type === TODO) {
             return {
		//TODO
             };
         }
       return state;
     };
 #+END_EXAMPLE

 Le but ici est de modifier monster qui se trouve dans notre state qui
 devrait ressembler à ceci :

 #+BEGIN_EXAMPLE javascript
     const initialState = {
         players: {
             1: { name: "John", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 1 },
             2: { name: "Jack", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 2 },
             3: { name: "Jessy", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 3 },
             4: { name: "Jenny", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 4 }
         },
         monster: { pv: 800, pvMax: 800}
     };
 #+END_EXAMPLE

Il nous faudra alors utiliser le spread opérator pour conserver l'état
immuable du store.

** DONE Utilisation de l'action avec les boutons

Vous avez réussis ? on va bien voir ... allez dans =ButtonCapacity.js=
pour faire appel à l'action.

Premierement, il va falloir lui aussi le re-factoriser.

Nous allons aussi utiliser le connecteur de dispatch react-redux :
 [[https://react-redux.js.org/using-react-redux/connect-mapdispatch][Connect: Dispatching Actions with mapDispatchToProps · React Redux]]

 #+BEGIN_EXAMPLE javascript
     //modification de src/Components/ButtonCapacity.js

     import React from 'react';
     import { connect } from 'react-redux'

     const mapDispatchToProps = dispatch => {
         return {
            hitMonster: //TODO
         };
     }

     const ButtonCapacityConnect = ({??}) => {
         const combat = () => {
           //TODO : use the dispatch
           console.log('aie !')
         }
         return (
             <button
	        type="button"
		onClick={() => combat()}
		className="btn btn-success material-tooltip-main "
             >
               hit
               <i className="fas fa-bomb"></i> 5
               <i className="fas fa-fire-alt"></i> - 5
             </button>
         )
     }

     export default connect( ?? )(ButtonCapacityDisplay);
 #+END_EXAMPLE

*Si tout marche bien, chaque click d'un bouton enlève des PV au monstre.*

 Allons coder le retour de bâton :

* Mise en pratique 2
** Action HIT_BACK
Maintenant que vous pouvez frapper le monstre, il va repliquer.

 - créer l'action "HIT_BACK"
 - intégrer le déclenchement de cette action dans la fonction combat de
   ButtonCapacity.js
 - au déclenchement de l'action, dans le reducer, retirer 5 pv à un
   joueur
 - faire en sorte que le joueur qui a tapé le monstre recoive les
   dégats en retour sur sa barre de pv
 - le spread opérator sera complexe, regardez cette documentation :

   - [[https://redux.js.org/recipes/structuring-reducers/immutable-update-patterns][Immutable Update Patterns · Redux]]
   - [[https://redux.js.org/recipes/using-object-spread-operator][Using Object Spread Operator · Redux]]

Petite ressource caché pour les curieux :
[[https://stackoverflow.com/questions/36265556/how-to-pass-nested-properties-with-spread-attributes-correctly-jsx]]
